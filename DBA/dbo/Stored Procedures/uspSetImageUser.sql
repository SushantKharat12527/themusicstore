﻿CREATE PROCEDURE [dbo].[uspSetImageUser]
	@Command Varchar(50),
	@SongId Numeric(18,0),
	@SongFile NVarchar(MAX),
	@SongName Varchar(50),
	
	@SongImage NVarchar(MAX),
	@SingerId Numeric(18,0),
	

	@Status int OUT

AS
	
		IF @Command='Add'
			BEGIN
				
				INSERT INTO tblSongs
				(
					SongId,
					SongName,
					SongFile,
					SongImage,
					SingerId
					
				)
				VALUES
				(
					@SongId,
					@SongName,
					@SongFile,
					@SongImage,
					@SingerId
				)


				SET @Status=1

			END

GO
