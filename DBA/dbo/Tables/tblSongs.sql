﻿CREATE TABLE [dbo].[tblSongs] (
    [SongId]      INT             IDENTITY (1, 1) NOT NULL,
    [SongName]    VARCHAR (50)    NULL,
    [SongGenre]   VARCHAR (50)    NULL,
    [SongImage]   VARBINARY (MAX) NULL,
    [SongAddress] VARBINARY (MAX) NULL,
    [SingerId]    INT             NULL,
    CONSTRAINT [PK_tblSongs] PRIMARY KEY CLUSTERED ([SongId] ASC)
);

