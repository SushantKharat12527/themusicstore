﻿CREATE TABLE [dbo].[tblCommunication] (
    [PhoneNo]  NUMERIC (18)  NOT NULL,
    [Email]    NVARCHAR (50) NULL,
    [PersonId] INT           NOT NULL,
    CONSTRAINT [PK_tblCommunication] PRIMARY KEY CLUSTERED ([PersonId] ASC)
);

