﻿CREATE TABLE [dbo].[tblPerson] (
    [PersonId]    INT             IDENTITY (1, 1) NOT NULL,
    [FirstName]   VARCHAR (MAX)   NULL,
    [LastName]    VARCHAR (MAX)   NULL,
    [Age]         INT             NULL,
    [PersonImage] VARBINARY (MAX) NULL,
    CONSTRAINT [PK_tblPerson] PRIMARY KEY CLUSTERED ([PersonId] ASC)
);

