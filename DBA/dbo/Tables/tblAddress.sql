﻿CREATE TABLE [dbo].[tblAddress] (
    [Country]  VARCHAR (50) NULL,
    [Sate]     VARCHAR (50) NULL,
    [City]     VARCHAR (50) NULL,
    [PersonId] INT          NOT NULL,
    CONSTRAINT [PK_tblAddress] PRIMARY KEY CLUSTERED ([PersonId] ASC)
);

