﻿CREATE TABLE [dbo].[tblImage] (
    [Id]          INT             IDENTITY (1, 1) NOT NULL,
    [Name]        VARCHAR (MAX)   NULL,
    [ContentType] VARCHAR (MAX)   NULL,
    [Data]        VARBINARY (MAX) NULL,
    [IsSelected]  BIT             NULL,
    CONSTRAINT [PK_tblImage] PRIMARY KEY CLUSTERED ([Id] ASC)
);

