﻿CREATE TABLE [dbo].[tblSongs] (
    [SongId]    INT             IDENTITY (1, 1) NOT NULL,
    [SongName]  VARCHAR (MAX)   NULL,
    [SongImage] VARBINARY (MAX) NULL,
    [SongFile]  VARBINARY (MAX) NULL,
    [SingerId]  INT             NULL,
    CONSTRAINT [PK_tblSongs] PRIMARY KEY CLUSTERED ([SongId] ASC)
);

