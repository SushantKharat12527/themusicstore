﻿CREATE TABLE [dbo].[tblSinger] (
    [SingerId]   INT           IDENTITY (1, 1) NOT NULL,
    [SingerName] VARCHAR (MAX) NULL,
    CONSTRAINT [PK_tblSinger] PRIMARY KEY CLUSTERED ([SingerId] ASC)
);

