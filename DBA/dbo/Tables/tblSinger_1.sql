﻿CREATE TABLE [dbo].[tblSinger] (
    [SingerId]   INT           IDENTITY (1, 1) NOT NULL,
    [SingerName] VARCHAR (MAX) NULL,
    CONSTRAINT [PK_tblSinger] PRIMARY KEY CLUSTERED ([SingerId] ASC),
    CONSTRAINT [FK_tblSinger_tblSinger] FOREIGN KEY ([SingerId]) REFERENCES [dbo].[tblSinger] ([SingerId])
);


GO
CREATE NONCLUSTERED INDEX [IX_tblSinger]
    ON [dbo].[tblSinger]([SingerId] ASC);

