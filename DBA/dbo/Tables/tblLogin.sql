﻿CREATE TABLE [dbo].[tblLogin] (
    [Username] NVARCHAR (50) NULL,
    [Password] NVARCHAR (50) NULL,
    [PersonId] INT           NOT NULL,
    [Token]    BIT           NULL,
    CONSTRAINT [PK_tblLogin] PRIMARY KEY CLUSTERED ([PersonId] ASC)
);

