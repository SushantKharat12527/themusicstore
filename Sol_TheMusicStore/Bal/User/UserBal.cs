﻿using System;
using System.Collections.Generic;
using Sol_TheMusicStore.Entities.User;
using System.Threading.Tasks;
using System.Linq;
using System.Web;
using Sol_TheMusicStore.Dal.User;

namespace Sol_TheMusicStore.Bal.User
{
    public class UserBal : UserDal
    {
        #region Constructor
        public UserBal():base()
        {

        }
        #endregion

        #region Public Method
        public    IEnumerable<SingerEntity> GetAllSinger()
        {
            try
            {
                return  base.GetAllSingerAsync();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public IEnumerable<SongEntity> GetAllSongs(int? Id)
        {
            try
            {
                return base.GetAllSongsAsync(Id);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public IEnumerable<SongEntity> GetLatestSongs()
        {
            try
            {
               return base.GetLatestSongsAsync();


            }
            catch (Exception)
            {

                throw;
            }
        }

        public IEnumerable<SongEntity> GetSelectedSongs(int? Id)
        {
            try
            {
                return base.GetSelectedSongAsync(Id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
    }
}