﻿using Sol_TheMusicStore.Dal.Admin;
using Sol_TheMusicStore.Entities.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_TheMusicStore.Bal.Admin
{
    public class AdminBal:AdminDal
    {
        #region Constructor
        public AdminBal() : base()
        {

        }
        #endregion

        #region Public Method
        public Boolean SetSongData(SongEntity songEntityObj)
        {
           
                try
                {
                    return  base.SetSongDataAsync(songEntityObj);
                }
                catch (Exception)
                {

                    throw;
                }
         
          
        }
        #endregion
    }
}