﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_TheMusicStore.Entities.User
{
    public class SongEntity
    {
        public int Song_Id { get; set; }
        public string Song_Name { get; set; }
        public string Song_Image { get; set; }
        public string Song_File { get; set; }

        public SingerEntity singerEntity { get; set; }
    }
}