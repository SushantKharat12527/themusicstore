﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_TheMusicStore.Entities.User
{
    public class SingerEntity
    {
        public int? Singer_Id { get; set; }                  

        public string Singer_Name { get; set; }
    }
}