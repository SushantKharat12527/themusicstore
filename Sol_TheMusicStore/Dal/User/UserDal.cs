﻿
using Sol_TheMusicStore.Dal.ORD.User;
using Sol_TheMusicStore.Entities.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_TheMusicStore.Dal.User
{
    public class UserDal
    {
        #region Declaration
        private  UserDataContext dc =null;
        int? status = null;
        
            #endregion
        #region Constructor
        public UserDal()
        {
            dc = new UserDataContext();
             
        }

        #endregion

        #region Public method
        public IEnumerable< SingerEntity> GetAllSingerAsync()
        {
            try
            {
               
                    var getSingerdata =
                        dc
                        ?.uspGetSinger("GetSinger",ref status)
                        ?.AsEnumerable()
                        ?.Select((leGetSingerResult) => new SingerEntity
                        {
                            Singer_Id = leGetSingerResult.SingerId,
                            Singer_Name = leGetSingerResult.SingerName

                        })
                        ?.ToList();

                    return getSingerdata;

               
            }
            catch (Exception)
            {

                throw;
            }

           
        }

        public IEnumerable<SongEntity> GetAllSongsAsync(int? id)
        {
            try
            {

                var getSongdata =
                    dc
                    ?.uspGetSong("GetSong", id, ref status)
                    ?.AsEnumerable()
                    ?.Select((leGetSongResult) => new SongEntity
                    {
                        Song_Id = leGetSongResult.SongId,
                        Song_File = leGetSongResult.SongFile,
                        Song_Image = leGetSongResult.SongImage,
                        Song_Name = leGetSongResult.SongName,
                        singerEntity = new SingerEntity()
                        {
                            Singer_Id = leGetSongResult.SingerId
                        }
                    })
                    ?.ToList();

                return getSongdata;


            }
            catch (Exception)
            {

                throw;
            }


        }

        public IEnumerable<SongEntity> GetLatestSongsAsync()
        {
            try
            {

                var getLatestSongs =
                    dc
                    ?.uspGetLatestSongs()
                    ?.AsEnumerable()
                    ?.Select((leGetLatestSongsResult) => new SongEntity
                    {
                        Song_Id = leGetLatestSongsResult.SongId,
                        Song_Name =leGetLatestSongsResult.SongName,
                        Song_File=leGetLatestSongsResult.SongFile,
                        Song_Image=leGetLatestSongsResult.SongImage,
                        singerEntity=new SingerEntity()
                        {
                            Singer_Id=leGetLatestSongsResult.SingerId
                        }
                        

                    })
                    ?.ToList();

                return getLatestSongs;


            }
            catch (Exception)
            {

                throw;
            }


        }

        public IEnumerable< SongEntity> GetSelectedSongAsync(int? id)
        {
            try
            {

                var getSelectedSongdata =
                    dc
                    ?.uspGetSelectedSong("GetSelectedSong", id, ref status)
                    ?.AsEnumerable()
                    ?.Select((leGetSongResult) => new SongEntity
                    {
                        Song_Id = leGetSongResult.SongId,
                        Song_File = leGetSongResult.SongFile,
                        Song_Image = leGetSongResult.SongImage,
                        Song_Name = leGetSongResult.SongName,
                        singerEntity = new SingerEntity()
                        {
                            Singer_Id = leGetSongResult.SingerId
                        }
                    })
                    ?.ToList();

                return getSelectedSongdata;


            }
            catch (Exception)
            {

                throw;
            }


        }
        #endregion
    }
}