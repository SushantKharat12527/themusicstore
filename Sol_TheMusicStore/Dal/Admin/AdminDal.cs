﻿using Sol_TheMusicStore.Dal.ORD.User;
using Sol_TheMusicStore.Entities.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_TheMusicStore.Dal.Admin
{
    public class AdminDal
    {
        private UserDataContext dc = null;

        #region Constructor
        public AdminDal()
        {
            dc = new UserDataContext();

        }

        #endregion

        #region Public Method
        public Boolean SetSongDataAsync(SongEntity songEntityObj)
        {
            int? status = null;
           

                var setUserQuery =
                    dc
                    ?.uspSetSongImage(
                            "Add",
                            songEntityObj?.Song_File,
                            songEntityObj?.Song_Name,
                            songEntityObj?.Song_Image,
                            songEntityObj?.singerEntity.Singer_Id,
                            ref status
                        );

                return (status == 1) ? true : false;

          
        }
        #endregion
    }
}