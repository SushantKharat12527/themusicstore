﻿using Sol_TheMusicStore.Bal.User;
using Sol_TheMusicStore.Dal.User;
using Sol_TheMusicStore.Entities.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_TheMusicStore.Shared
{
    public partial class masterMenu : System.Web.UI.MasterPage
    {
        #region Declaration
        // Create an Instance of User Bal Object
        UserBal userBalObj = new UserBal();
        DataList dataList = new DataList();
        List<SongEntity> Songlist = new List<SongEntity>();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack == true)
            {
                //Bind
                SingerBinding = userBalObj.GetAllSinger();
            }
        }

        protected void DdlSinger_SelectedIndexChanged(object sender, EventArgs e)
        {
            //binding Songs to ddlSong
            SongBinding = userBalObj.GetAllSongs(Convert.ToInt32(ddlSinger.SelectedValue));
        }

        protected void ddlSongs_SelectedIndexChanged(object sender, EventArgs e)
        {
            Songlist = userBalObj.GetSelectedSongAsync(Convert.ToInt32(ddlSongs.SelectedValue)).ToList();
            dataList.datalistBinding(Songlist);

            
        }


        private dynamic SingerBinding
        {
            get
            {
                return ddlSinger.SelectedValue;

            }
            set
            {
                List<SingerEntity> singerEntityObj = value;


                ddlSinger.AppendDataBoundItems = true;
                ddlSinger.Items.Clear();
                ddlSinger.Items.Insert(0, new ListItem("--Select Singer--", "0"));
                ddlSinger.SelectedIndex = 0;

                ddlSinger.DataSource = singerEntityObj;
                ddlSinger.DataTextField = "Singer_Name";
                ddlSinger.DataValueField = "Singer_Id";
                ddlSinger.DataBind();
            }
        }

        private dynamic SongBinding
        {
            get
            {
                return ddlSongs.SelectedValue;

            }
            set
            {
                List<SongEntity> songEntityObj = value;


                ddlSongs.AppendDataBoundItems = true;
                ddlSongs.Items.Clear();
                ddlSongs.Items.Insert(0, new ListItem("--Select Song--", "0"));
                ddlSongs.SelectedIndex = 0;
                ddlSongs.DataTextField = null;
                ddlSongs.DataSource = songEntityObj;
                ddlSongs.DataTextField = "Song_Name";
                ddlSongs.DataValueField = "Song_Id";
                ddlSongs.DataBind();
            }
        }

        //private dynamic datalistBinding
        //{
        //    get
        //    {
        //        return dataListLatestSongs;
        //    }
        //    set
        //    {
        //        List<SongEntity> songEntityObj = value;

        //        this.dataListLatestSongs.DataSource = songEntityObj;

        //        dataListLatestSongs.DataBind();
        //    }
        //}

    }
    
    
}