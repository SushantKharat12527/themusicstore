﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/masterMenu.Master" AutoEventWireup="true" CodeBehind="DataList.aspx.cs" Inherits="Sol_TheMusicStore.Shared.DataList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
   #imgTd{
        text-align: center;
   }
   #nameTd{
           text-align: center;
   }
    #datalist{
        float: left;
   width: 500px;
    height: auto;
    }
    td{
        height:75px;
        width:50px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="datalist">
<asp:DataList  ID="dataListLatestSongs" runat="server" RepeatColumns="3" CellPadding="3" BackColor="#99CCFF" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellSpacing="2" GridLines="Both" Height="453px" Width="500px" CssClass="dataListLatestSongs" Font-Names="Arial" Font-Size="X-Large" ForeColor="#FF6666">
    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
    <ItemStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
    <ItemTemplate>
        <table >
            
            <tr ><td id="nameTd"> <%#Eval("Song_Name") %></td></tr>
            <tr><td id="imgTd">
                
                <asp:Image ID="Image1" ImageUrl='<%# Eval("Song_Image") %>' runat="server" Height="100"
                        Width="100" />
                </td></tr>
            <tr><td>
                
                <audio controls="controls">
                <source src=' <%#Eval("Song_File") %>' runat="server" />
                    </audio></td></tr>
        </table>
    </ItemTemplate>

    <SelectedItemStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />

</asp:DataList>    
    </div>

    </asp:Content>
