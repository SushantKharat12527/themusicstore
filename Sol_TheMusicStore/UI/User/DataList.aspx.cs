﻿using Sol_TheMusicStore.Bal.User;
using Sol_TheMusicStore.Entities.User;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_TheMusicStore.Shared
{
    public partial class DataList : System.Web.UI.Page
    {
        #region Declaration

        private UserBal userBalObj = new UserBal();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            List<SongEntity> SongEntityObj = new List<SongEntity>();
              SongEntityObj= userBalObj.GetLatestSongs().ToList();
            this.datalistBinding(SongEntityObj);
            //dataListLatestSongs.DataSource = userBalObj.GetLatestSongs();
            //dataListLatestSongs.DataBind();

        }

     public void datalistBinding(List< SongEntity> songEntityObj)
        {
            
            
            dataListLatestSongs.DataSource = songEntityObj;
            dataListLatestSongs.DataBind();
        }
      
    }
}