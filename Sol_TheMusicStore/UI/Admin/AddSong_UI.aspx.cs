﻿using Sol_TheMusicStore.Bal.Admin;
using Sol_TheMusicStore.Bal.User;
using Sol_TheMusicStore.Dal.User;
using Sol_TheMusicStore.Entities.User;
using Sol_TheMusicStore.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_TheMusicStore.UI.User
{
    public partial class AddSong_UI : System.Web.UI.Page
    {
        #region Declaration
        private AdminBal adminDalObj = null;
        private UserBal userBalObj = new UserBal();
        #endregion 

        protected void Page_Load(object sender, EventArgs e)
        {
            //Bind
            SingerBinding = userBalObj.GetAllSinger();
        }

        protected async void btnUpload_Click(object sender, EventArgs e)
        {

            // create an instance of User Dal Object
            adminDalObj = new AdminBal();

            await UploadImageBind();
            await this.UploadSongBind();
            // Set Song Data
             adminDalObj.SetSongData(await this.DataMapping());
        }

        private async Task UploadSongBind()
        {
            await Task.Run(() =>
            {

                // Invoke SongUploadBinding Property

                var virtualSongPath = this.SongUploadBinding;

                // Bind Song to player 

                this.SongPlayerBinding = virtualSongPath;

            });
        }

        private async Task UploadImageBind()
        {
            await Task.Run(() =>
            {

                // Invoke ImageUploadBinding Property

                var virtualImagePath = this.ImageUploadBinding;

                // Bind Image 

                this.SongImageBinding = virtualImagePath;

            });
        }

        private async Task<SongEntity> DataMapping()
        {
            return await Task.Run(() =>
            {

                var songEntityObj = new SongEntity()
                {
                    Song_Name = this.SongNameBinding,
                    Song_File = this.SongPlayerBinding,
                    Song_Image = this.SongImageBinding,
                    singerEntity = new SingerEntity()
                    {
                        Singer_Id = Convert.ToInt32(this.SingerIdBinding)
                        //Singer_Id = Convert.ToInt32(ddlSingersList)
                    }
                };

                return songEntityObj;
            });
        }

        private string SongPlayerBinding
        {
            get
            {
                return
                       sngPlayer.Src;
            }
            set
            {
                string VirtualPath = value;

                sngPlayer.Src = VirtualPath;
            }
        }

        private String SongUploadBinding
        {
            get
            {
                String virtualPath = null;
                if (songFileUpload.HasFile)
                {
                    // C:\Users\Ideators2\Desktop\Sol_UploadControl_ImageControl\Sol_UploadControl_ImageControl\Images
                    string physicalPath =
                        Server.MapPath(
                            new StringBuilder()
                                .Append("~/")
                                .Append("Songs/")
                                .Append(songFileUpload.FileName)
                                .ToString()
                            );

                    // Save file in Server
                    songFileUpload.SaveAs(physicalPath);

                    // Set Virtual Path
                    // ~\Images\img.jpg

                    virtualPath =
                    new StringBuilder()
                    .Append("~/Songs/")
                    .Append(songFileUpload.FileName)
                    .ToString();


                }

                return virtualPath;
            }
        }

        private int? SingerIdBinding
        {
            get
            {

                return
                    Convert.ToInt32(Server.HtmlEncode(ddlSingersList.SelectedValue));
            }
        }
        private String SongNameBinding
        {
            get
            {
                return
                    Server.HtmlEncode(txtSongName.Text.Trim());
            }
        }

        private String SongImageBinding
        {
            get
            {
                return
                       imgSongPic.ImageUrl;
            }
            set
            {
                imgSongPic.ImageUrl = value;
            }
        }

        private String ImageUploadBinding
        {
            get
            {
                String virtualPath = null;
                if (imageFileUpload.HasFile)
                {
                    // C:\Users\Ideators2\Desktop\Sol_UploadControl_ImageControl\Sol_UploadControl_ImageControl\Images
                    string physicalPath =
                        Server.MapPath(
                            new StringBuilder()
                                .Append("~/")
                                .Append("Images/")
                                .Append(imageFileUpload.FileName)
                                .ToString()
                            );

                    // Save file in Server
                    imageFileUpload.SaveAs(physicalPath);

                    // Set Virtual Path
                    // ~\Images\img.jpg

                    virtualPath =
                    new StringBuilder()
                    .Append("~/Images/")
                    .Append(imageFileUpload.FileName)
                    .ToString();


                }

                return virtualPath;
            }
        }

        private dynamic SingerBinding
        {
            get
            {
                return ddlSingersList.SelectedValue;

            }
            set
            {
                List<SingerEntity> singerEntityObj = value;


                ddlSingersList.AppendDataBoundItems = true;
                ddlSingersList.Items.Clear();
                ddlSingersList.Items.Insert(0, new ListItem("--Select Singer--", "0"));
                ddlSingersList.SelectedIndex = 0;

                ddlSingersList.DataSource = singerEntityObj;
                ddlSingersList.DataTextField = "Singer_Name";
                ddlSingersList.DataValueField = "Singer_Id";
                ddlSingersList.DataBind();
            }
        }


    }
}