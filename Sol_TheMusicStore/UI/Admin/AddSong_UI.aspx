﻿<%@ Page Title=""  Async="true" Language="C#" MasterPageFile="~/Shared/masterMenu.Master" AutoEventWireup="true" CodeBehind="AddSong_UI.aspx.cs" Inherits="Sol_TheMusicStore.UI.User.AddSong_UI" %>


   
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 
    
    
    <style type="text/css">
        body {
            background-color: coral;
        }

        .img-Circle {
            border: 2px;
            border-color: gray;
            border-style: dashed;
            border-radius: 50%;
            height: 250px;
            width: 250px;
        }

        .sng-player {
            border: 2px;
            border-color: gray;
            border-style: dashed;
            border-radius: 50%;
            height: 250px;
            width: 250px;
        }

        #tblContent {
            text-align: center;
           
        }

        #imageSongPic {
            height: 255px;
            width: 255px;
            margin: auto;
        }

        #songPlayer {
            height: 35px;
            width: 300px;
            margin: auto;
        }

        .TextboxStyle {
            height: 25px;
            width: 360px;
            font-size: 20px;
            font-family: cursive;
        }
     
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div>
         
            <table id="tblContent"  runat="server" border="1">
                <tr>
                    <td>
                        <asp:TextBox ID="txtSongName" placeholder="Song Name" runat="server" CssClass="TextboxStyle"></asp:TextBox>
                    </td>

                </tr>

                <tr>
                    <td>
                        <asp:DropDownList ID="ddlSingersList" runat="server" AutoPostBack="false"></asp:DropDownList>
                    </td>

                </tr>

                <tr>
                    <td>Upload Image:
                        <asp:FileUpload ID="imageFileUpload" runat="server" />
                    </td>
                </tr>
                <tr>
                </tr>
                <tr>
                    <td>Upload Song File:   
                        <asp:FileUpload ID="songFileUpload" runat="server" />
                    </td>
                </tr>

                <tr>
                    <td>
                        <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="btnUpload_Click" />
                    </td>
                </tr>




            </table>
            <br />
            <br />
            <div id="imageSongPic">
                <asp:Image ID="imgSongPic" runat="server" CssClass="img-Circle" />
            </div>

        </div>
        <div id="songPlayer">
            <audio controls="controls">
                <source id="sngPlayer" runat="server" class="sng-player" />
            </audio>
        </div>
       
</asp:Content>
