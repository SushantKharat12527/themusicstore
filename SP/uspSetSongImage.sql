Alter PROCEDURE uspSetSongImage
	@Command Varchar(50),
	
	@SongFile NVarchar(MAX),
	@SongName Varchar(50),
	
	@SongImage NVarchar(MAX),
	@SingerId Numeric(18,0),
	

	@Status int OUT

AS
	
		IF @Command='Add'
			BEGIN
				
				INSERT INTO tblSongs
				(
					
					SongName,
					SongFile,
					SongImage,
					SingerId
					
				)
				VALUES
				(
					
					@SongName,
					@SongFile,
					@SongImage,
					@SingerId
				)


				SET @Status=1

			END

GO
