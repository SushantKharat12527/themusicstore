USE [TheMusicStore]
GO
/****** Object:  StoredProcedure [dbo].[uspGetSong]    Script Date: 22-04-2018 22:14:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[uspGetSong]
	@Command Varchar(50),
	
	
	@SingerId Numeric(18,0)=null,
	

	@Status int OUT

AS
	

 if @Command='GetSelectedSong'

Begin

select * from tblSongs
where tblSongs.SingerId=@SingerId
return
set @Status=1
End